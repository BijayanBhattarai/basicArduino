#include<LiquidCrystal.h>
#define LM335 0
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
 
float kelvin, Celcius;
void setup() {
  lcd.begin(16,2);
  lcd.print("kelvin k = "); //Print at cursor Location
  lcd.setCursor(0,1);
  lcd.print("Celcius C =");
  lcd.setCursor(0,2);
  //goto first column (column 0) and second line (line 1)
  
  // put your setup code here, to run once:

}
void loop() {
  delay (1000);
  kelvin = analogRead(LM335) * 0.489 - 1;
  lcd.setCursor(12,0); //move the cursor to position 8 on row 1
  lcd.print(kelvin); //print the temperature in Celsius 
  Celcius = kelvin - 273;
  lcd.setCursor(12,1);
  lcd.print(Celcius);
   // put your main code here, to run repeatedly:
}
