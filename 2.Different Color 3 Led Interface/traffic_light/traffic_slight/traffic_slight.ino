int red = 13;
int yellow = 12;
int green = 11;
void setup()
{
 pinMode(red,OUTPUT);
 pinMode(yellow,OUTPUT);
 pinMode(green,OUTPUT);
  // put your setup code here, to run once:
}

void loop() {
  // red light
  digitalWrite(red,HIGH);
  delay(1000);
  digitalWrite(red,LOW);
  digitalWrite(yellow,HIGH);
  // yellow light
  delay(1000);
  digitalWrite(yellow,LOW);
  digitalWrite(green,HIGH);
  delay(1000);
  // GREEN LIGHT
  digitalWrite(green,LOW);
   // put your main code here, to run repeatedly:

}
