int pin = 13;
void setup() 
{
 pinMode(pin, OUTPUT); // put your setup code here, to run once:

}

void loop() {
  digitalWrite(pin,HIGH);
  delay(1000);
  digitalWrite(pin,LOW);
  delay(1000);
  // put your main code here, to run repeatedly:
}
